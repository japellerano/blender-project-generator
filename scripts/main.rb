#!/usr/bin/env ruby

# Require
require 'fileutils'

gen_path = FileUtils.pwd
gen_files = gen_path  + '/files/'

# Welcome statement.
puts 'Welcome to Blend Project Generator!'
puts 'Please enter where you wish to start your new project!'

# proj_path 
proj_path = gets.chomp

# Project Name
puts 'Please Enter Your Project Name:'
proj_name = gets.chomp

puts 'Do you want your project name to be the name of the folder? (y/n)'
proj_ans = gets.chomp

if proj_ans == "Y" || proj_ans == "y"
  proj_folder = proj_name
elsif proj_ans == "N" || proj_ans == "n"
  puts 'What do you want the folder to be named?'
  proj_folder = gets
else
  puts 'Error, incorrect input.'
end

# TODO - check for slashes
new_proj_path = proj_path + proj_folder
# Folder list
# Main Folder
# - fin
# - lib
#   - ref
#   - rend
#   - rec
#   - aud
# - prod
#   - 3d
#   - tex
#   - concept

puts 'Creating Project Folder: ' + new_proj_path + '!'
FileUtils.mkdir_p(proj_path + proj_folder)

folders = ['fin', 'lib', 'prod']
lib_folders = ['ref', 'rend', 'rec', 'aud']
prod_folders = ['3d', 'tex', 'concept']

folders.each do |f|
  FileUtils.mkdir_p(new_proj_path + '/' + f)
  puts 'Creating sub folders: ' + f
end

lib_folders.each do |l|
  FileUtils.mkdir_p(new_proj_path + '/lib/' + l)
  puts 'Creating lib folders: ' + l
end

prod_folders.each do |p|
  FileUtils.mkdir_p(new_proj_path + '/prod/' + p)
  puts 'Creating prod folders: ' + p
end

puts 'Copying gitignore!'
FileUtils.cp(gen_files + 'gitignore', new_proj_path + '/.gitignore')

puts 'Creating README!'
# FileUtils.touch(new_proj_path + '/README.md')
File.open(new_proj_path + '/README.md', "a") do |line|
  line.puts "## " + proj_name
end
